Пример использования модуля:

/**
 * Payment form.
 */
function mymodule_payment_form($form, &$form_submit) {
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Pay'),
  );
  
  return $form;
}


/**
 * Payment form submit callback.
 */
function mymodule_payment_form_submit($form, &$form_submit) {
  $form_state['redirect'] = assistru_get_redirect_url(array(
    'OrderNumber' => $order_number,       // Required
    'OrderAmount' => $order_amount,       // Required
    'Lastname' => $customer_lastname,     // Optional
    'Firstname' => $customer_firstname,   // Optional
    'Middlename' => $customer_middlename, // Optional
    'Email' => $customer_email,           // Optional
  ));
}

/**
 * Implements hook_assistru_payment_success().
 */
function mymodule_assistru_payment_success($data) {
  $order = order_load($data['ordernumber']);
  $order->status = 1;
  $order->save();
}
