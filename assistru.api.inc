<?php

/**
 * API request.
 */
function assistru_api_request($path, $params) {
  $result = drupal_http_request('https://' . variable_get('assistru_host') . '/' . $path, array(
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    'method' => 'POST',
    'data' => drupal_http_build_query($params),
  ));

  return $result->data;
}
